package cn.net.susan.sensitive;

import cn.hutool.core.text.CharSequenceUtil;
import org.springframework.stereotype.Service;

/**
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/23 下午6:04
 */
@Service
public class CustomMaskService implements ICustomMaskService {
    @Override
    public String maskData(String data) {
        return CharSequenceUtil.hide(data, 1, data.length());
    }
}
