package cn.net.susan.dto.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 城市DTO
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/2/26 下午12:01
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CityDTO {

    /**
     * ip
     */
    private String ip;

    /**
     * 国家
     */
    private String country;

    /**
     * 省份
     */
    private String province;

    /**
     * 所在城市
     */
    private String city;
}
