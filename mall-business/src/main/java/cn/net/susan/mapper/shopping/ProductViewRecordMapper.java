package cn.net.susan.mapper.shopping;

import cn.net.susan.entity.shopping.ProductViewRecordConditionEntity;
import cn.net.susan.entity.shopping.ProductViewRecordEntity;
import java.util.List;

import cn.net.susan.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品浏览记录 mapper
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-09-04 15:12:10
 */
public interface ProductViewRecordMapper extends BaseMapper<ProductViewRecordEntity, ProductViewRecordConditionEntity> {
	/**
     * 查询商品浏览记录信息
     *
     * @param id 商品浏览记录ID
     * @return 商品浏览记录信息
     */
	ProductViewRecordEntity findById(Long id);

	/**
     * 添加商品浏览记录
     *
     * @param productViewRecordEntity 商品浏览记录信息
     * @return 结果
     */
	int insert(ProductViewRecordEntity productViewRecordEntity);

	/**
     * 修改商品浏览记录
     *
     * @param productViewRecordEntity 商品浏览记录信息
     * @return 结果
     */
	int update(ProductViewRecordEntity productViewRecordEntity);

    /**
     * 批量删除商品浏览记录
     *
     * @param ids id集合
     * @param entity 商品浏览记录实体
     * @return 结果
     */
    int deleteByIds(@Param("ids") List<Long> ids, @Param("entity") ProductViewRecordEntity entity);

    /**
     * 批量查询商品浏览记录信息
     *
     * @param ids ID集合
     * @return 商品浏览记录信息
    */
    List<ProductViewRecordEntity> findByIds(List<Long> ids);
}
