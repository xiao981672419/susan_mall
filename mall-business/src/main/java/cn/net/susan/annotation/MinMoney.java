package cn.net.susan.annotation;

/**
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/6/11 下午4:12
 */

import cn.net.susan.valid.MinMoneyConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 最小值约束.
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/6/11 下午4:13
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MinMoneyConstraintValidator.class)
public @interface MinMoney {
    /**
     * message.
     *
     * @return
     */
    String message() default "{minMoney.message.error}";

    /**
     * min value.
     *
     * @return
     */
    double value() default 0;

    /**
     * group.
     *
     * @return
     */
    Class<?>[] groups() default {};

    /**
     * payload.
     *
     * @return
     */
    Class<? extends Payload>[] payload() default {};
}
