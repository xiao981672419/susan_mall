package cn.net.susan.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 防止重复提交注解
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/2/12 下午8:49
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
    public @interface RepeatSubmit {

    /**
     * 限制时间，单位秒
     *
     * @return 秒
     */
    int second() default 5;
}
