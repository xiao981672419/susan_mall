package cn.net.susan.entity.mall;

import cn.net.susan.annotation.ValidSensitiveWordField;
import cn.net.susan.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 属性实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-05-09 14:43:55
 */
@ApiModel("属性实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AttributeEntity extends BaseEntity {


	/**
	 * 属性名称
	 */
	@ValidSensitiveWordField
	@ApiModelProperty("属性名称")
	private String name;
}
