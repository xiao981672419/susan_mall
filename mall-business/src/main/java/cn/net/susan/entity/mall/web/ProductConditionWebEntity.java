package cn.net.susan.entity.mall.web;

import cn.net.susan.entity.RequestPageEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 商品查询条件实体
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-05-09 14:43:56
 */
@ApiModel("商品查询条件实体")
@Data
public class ProductConditionWebEntity extends RequestPageEntity {


    /**
     * 分类ID
     */
    @ApiModelProperty("分类ID")
    private Long categoryId;

    /**
     * 品牌ID
     */
    @ApiModelProperty("品牌ID")
    private Long brandId;

    /**
     * 单位ID
     */
    @ApiModelProperty("单位ID")
    private Long unitId;


    /**
     * 关键字
     */
    @ApiModelProperty("关键字")
    private String keyword;

    /**
     * 类型
     */
    @ApiModelProperty("类型")
    private Integer type;
}
