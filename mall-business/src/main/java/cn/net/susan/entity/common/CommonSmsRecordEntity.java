package cn.net.susan.entity.common;

import cn.net.susan.annotation.Sensitive;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cn.net.susan.entity.BaseEntity;
import java.util.Date;

/**
 * 短信发送记录实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-11-08 13:03:15
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommonSmsRecordEntity extends BaseEntity {

	/**
	 * 手机号
	 */
	@Sensitive
	private String phone;

	/**
	 * 验证码
	 */
	private String smsCode;

	/**
	 * 有效期
	 */
	private Integer expireSecond;

	/**
	 * 发送时间
	 */
	private Date sendTime;
}
