package cn.net.susan.entity.aftersale;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cn.net.susan.entity.BaseEntity;

/**
 * 退货单图片实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-10-28 15:30:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RefundPhotoEntity extends BaseEntity {


	/**
	 * 退货单ID
	 */
	private Long refundId;

	/**
	 * 图片名称
	 */
	private String name;

	/**
	 * 图片url
	 */
	private String url;
}
