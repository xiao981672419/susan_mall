package cn.net.susan.entity.pay.web;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 支付web实体
 *
 * @author 苏三
 * @date 2024/9/24 下午9:30
 */
@ApiModel("支付web实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PayWebEntity {
    /**
     * 订单code
     */
    private String tradeCode;
}
