package cn.net.susan.entity.shopping;

import cn.net.susan.entity.mall.UserProductEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cn.net.susan.entity.BaseEntity;

import javax.validation.constraints.NotNull;

/**
 * 商品收藏实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-09-04 15:12:10
 */
@Data
public class ProductFavoritesEntity extends UserProductEntity {

}
