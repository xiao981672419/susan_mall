package cn.net.susan.entity.sys;

import cn.net.susan.entity.RequestPageEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;

/**
 * 部门查询条件实体
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-03-25 21:41:03
 */
@ApiModel("部门查询条件实体")
@Data
public class DictDetailConditionEntity extends RequestPageEntity {

    /**
     * 数据字典名称
     */
    @NotEmpty(message = "数据字典名称不能为空")
    @ApiModelProperty("数据字典名称")
    private String dictName;


    /**
     * ID
     */
    @ApiModelProperty("ID")
    private Long id;

    /**
     * 数据字典id
     */
    @ApiModelProperty("数据字典id")
    private Long dictId;

    /**
     * 数据字典id集合
     */
    @ApiModelProperty("数据字典id集合")
    private List<Long> dictIdList;

    /**
     * 值
     */
    @ApiModelProperty("值")
    private String value;

    /**
     * 文本
     */
    @ApiModelProperty("文本")
    private String label;

    /**
     * 创建人ID
     */
    @ApiModelProperty("创建人ID")
    private Long createUserId;

    /**
     * 创建人名称
     */
    @ApiModelProperty("创建人名称")
    private String createUserName;

    /**
     * 创建日期
     */
    @ApiModelProperty("创建日期")
    private Date createTime;

    /**
     * 修改人ID
     */
    @ApiModelProperty("修改人ID")
    private Long updateUserId;

    /**
     * 修改人名称
     */
    @ApiModelProperty("修改人名称")
    private String updateUserName;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private Date updateTime;

    /**
     * 是否删除 1：已删除 0：未删除
     */
    @ApiModelProperty("是否删除 1：已删除 0：未删除")
    private Integer isDel;
}
