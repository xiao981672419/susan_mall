package cn.net.susan.service.common;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import cn.net.susan.enums.CommonJobOperateTypeEnum;
import cn.net.susan.exception.BusinessException;
import cn.net.susan.helper.MqHelper;
import cn.net.susan.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import cn.net.susan.mapper.common.CommonJobMapper;
import cn.net.susan.entity.common.CommonJobConditionEntity;
import cn.net.susan.entity.common.CommonJobEntity;
import cn.net.susan.entity.ResponsePageEntity;
import cn.net.susan.util.AssertUtil;
import cn.net.susan.util.FillUserUtil;
import cn.net.susan.mapper.BaseMapper;
import org.springframework.util.StringUtils;

import static cn.net.susan.config.RabbitConfig.DYNAMIC_JOB_EXCHANGE;
import static cn.net.susan.config.RabbitConfig.DYNAMIC_JOB_ROUTING_KEY_PREFIX;
import static cn.net.susan.config.RabbitConfig.EXCEL_EXPORT_QUEUE_ROUTING_KEY_PREFIX;

/**
 * 定时任务 服务层
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-04-30 15:09:06
 */
@Slf4j
@Service
public class CommonJobService extends BaseService<CommonJobEntity, CommonJobConditionEntity> {

    @Autowired
    private CommonJobMapper commonJobMapper;
    @Autowired
    private MqHelper mqHelper;

    @Value("${mall.mgt.commonJobTopic:COMMON_JOB_TOPIC}")
    private String commonJobTopic;

    /**
     * 查询定时任务信息
     *
     * @param id 定时任务ID
     * @return 定时任务信息
     */
    public CommonJobEntity findById(Long id) {
        return commonJobMapper.findById(id);
    }

    /**
     * 根据条件分页查询定时任务列表
     *
     * @param commonJobConditionEntity 定时任务信息
     * @return 定时任务集合
     */
    public ResponsePageEntity<CommonJobEntity> searchByPage(CommonJobConditionEntity commonJobConditionEntity) {
        return super.searchByPage(commonJobConditionEntity);
    }

    /**
     * 立即执行定时任务
     *
     * @param commonJobEntity 定时任务实体
     */
    public void runNow(CommonJobEntity commonJobEntity) {
        changeJob(commonJobEntity, CommonJobOperateTypeEnum.RUN_NOW);
    }

    /**
     * 恢复定时任务
     *
     * @param commonJobEntity 定时任务实体
     */
    public void resume(CommonJobEntity commonJobEntity) {
        CommonJobEntity jobEntity = checkChangeJobParam(commonJobEntity);
        jobEntity.setPauseStatus(false);
        FillUserUtil.fillUpdateUserInfo(jobEntity);
        commonJobMapper.update(jobEntity);

        jobEntity.setOperateTypeEnum(CommonJobOperateTypeEnum.RESUME);
        sendDynamicJobMessage(jobEntity);
    }

    /**
     * 暂停定时任务
     *
     * @param commonJobEntity 定时任务实体
     */
    public void pause(CommonJobEntity commonJobEntity) {
        CommonJobEntity jobEntity = checkChangeJobParam(commonJobEntity);
        jobEntity.setPauseStatus(true);
        FillUserUtil.fillUpdateUserInfo(jobEntity);
        commonJobMapper.update(jobEntity);

        jobEntity.setOperateTypeEnum(CommonJobOperateTypeEnum.PAUSE);
        sendDynamicJobMessage(jobEntity);
    }

    /**
     * 立即执行订单任务
     *
     * @param commonJobEntity 定时任务实体
     */
    private void changeJob(CommonJobEntity commonJobEntity, CommonJobOperateTypeEnum operateTypeEnum) {
        CommonJobEntity jobEntity = checkChangeJobParam(commonJobEntity);
        jobEntity.setOperateTypeEnum(operateTypeEnum);
        sendDynamicJobMessage(jobEntity);
    }

    private CommonJobEntity checkChangeJobParam(CommonJobEntity commonJobEntity) {
        AssertUtil.notNull(commonJobEntity.getId(), "id不能为空");
        CommonJobEntity jobEntity = commonJobMapper.findById(commonJobEntity.getId());
        AssertUtil.notNull(jobEntity, "当前job不存在");
        return jobEntity;
    }

    /**
     * 新增定时任务
     *
     * @param commonJobEntity 定时任务信息
     * @return 结果
     */
    public int insert(CommonJobEntity commonJobEntity) {
        checkParam(commonJobEntity);
        commonJobEntity.setPauseStatus(false);
        int insert = commonJobMapper.insert(commonJobEntity);
        commonJobEntity.setOperateTypeEnum(CommonJobOperateTypeEnum.NEW);
        sendDynamicJobMessage(commonJobEntity);
        return insert;
    }

    private void sendDynamicJobMessage(CommonJobEntity commonJobEntity) {
        mqHelper.send(commonJobTopic, commonJobEntity);
    }

    private String getRoutingKey(Long id) {
        return String.format("%s%s", DYNAMIC_JOB_ROUTING_KEY_PREFIX, id);
    }

    /**
     * 修改定时任务
     *
     * @param commonJobEntity 定时任务信息
     * @return 结果
     */
    public int update(CommonJobEntity commonJobEntity) {
        AssertUtil.notNull(commonJobEntity.getId(), "id不能为空");
        checkParam(commonJobEntity);
        int update = commonJobMapper.update(commonJobEntity);
        commonJobEntity.setOperateTypeEnum(CommonJobOperateTypeEnum.UPDATE);
        sendDynamicJobMessage(commonJobEntity);
        return update;
    }

    private void checkParam(CommonJobEntity commonJobEntity) {
        if (StringUtils.hasLength(commonJobEntity.getCronExpression())) {
            try {
                new CronExpression(commonJobEntity.getCronExpression());
            } catch (Exception e) {
                log.info("cron解析失败，原因：", e);
                throw new BusinessException("cron表达式错误");
            }
        }

        commonJobEntity.setBeanName(commonJobEntity.getBeanName().trim());

        CommonJobConditionEntity commonJobConditionEntity = new CommonJobConditionEntity();
        commonJobConditionEntity.setBeanName(commonJobEntity.getBeanName());
        List<CommonJobEntity> commonJobEntities = commonJobMapper.searchByCondition(commonJobConditionEntity);
        if (Objects.nonNull(commonJobEntity.getId())) {
            Optional<CommonJobEntity> optional = commonJobEntities.stream()
                    .filter(x -> !x.getId().equals(commonJobEntity.getId())).findAny();
            if (optional.isPresent()) {
                throw new BusinessException("该定时任务已存在，请重新修改");
            }
        } else {
            if (CollectionUtils.isNotEmpty(commonJobEntities)) {
                throw new BusinessException("该定时任务已存在，请勿重复添加");
            }
        }
    }

    /**
     * 批量删除定时任务对象
     *
     * @param ids 系统ID集合
     * @return 结果
     */
    public int deleteByIds(List<Long> ids) {
        List<CommonJobEntity> entities = commonJobMapper.findByIds(ids);
        AssertUtil.notEmpty(entities, "定时任务已被删除");

        CommonJobEntity entity = new CommonJobEntity();
        FillUserUtil.fillUpdateUserInfo(entity);
        int delete = commonJobMapper.deleteByIds(ids, entity);

        for (CommonJobEntity commonJobEntity : entities) {
            commonJobEntity.setOperateTypeEnum(CommonJobOperateTypeEnum.DELETE);
            sendDynamicJobMessage(commonJobEntity);
        }
        return delete;
    }

    @Override
    protected BaseMapper getBaseMapper() {
        return commonJobMapper;
    }

}
