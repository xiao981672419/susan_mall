package cn.net.susan.sharding;

import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * 表自定义分片算法
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/6/6 下午8:41
 */
@Component
@Slf4j
public class OrderTablePreciseShardingAlgorithm implements PreciseShardingAlgorithm<String> {
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<String> preciseShardingValue) {
        // 获取分片键的值
        String orderCode = preciseShardingValue.getValue();
        int hashCode = orderCode.hashCode();
        if (hashCode < 0) {
            hashCode = 0 - hashCode;
        }
        String logicTableName = preciseShardingValue.getLogicTableName();
        String physicalTableName = logicTableName + "_" + (hashCode % 2);
        log.info("分片的hashCode: {},逻辑表名 logicTableName: {}", hashCode, physicalTableName);
        if (collection.contains(physicalTableName)) {
            return physicalTableName;
        }
        return physicalTableName;
    }
}
