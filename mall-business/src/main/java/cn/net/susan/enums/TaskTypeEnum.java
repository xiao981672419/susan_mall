package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 任务类型
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/1/29 下午4:46
 */
@Getter
@AllArgsConstructor
public enum TaskTypeEnum {

    EXPORT_EXCEL(1, "通用excel数据导出"),

    SEND_EMAIL(2, "发送邮件");

    /**
     * 枚举值
     */
    private Integer value;


    /**
     * 枚举描述
     */
    private String desc;
}
