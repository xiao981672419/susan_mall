package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 图片类型
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/5 下午2:46
 */
@Getter
@AllArgsConstructor
public enum PhotoTypeEnum {

    /**
     * 封面
     */
    COVER(1, "封面"),

    /**
     * 轮播图
     */
    SWIPER(2, "轮播图");

    /**
     * 枚举值
     */
    private Integer value;


    /**
     * 枚举描述
     */
    private String desc;
}
