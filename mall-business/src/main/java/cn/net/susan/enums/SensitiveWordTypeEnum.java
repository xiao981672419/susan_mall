package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 敏感词类型
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/18 下午4:46
 */
@Getter
@AllArgsConstructor
public enum SensitiveWordTypeEnum {

    POLITICS(1, "政治"),

    LAWLESS(2, "违法"),

    SEX(3, "色情"),

    AD(4, "广告"),

    WEBSITE(5, "网址");

    /**
     * 枚举值
     */
    private Integer value;


    /**
     * 枚举描述
     */
    private String desc;
}
