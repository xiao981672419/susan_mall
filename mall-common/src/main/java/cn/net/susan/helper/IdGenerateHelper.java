package cn.net.susan.helper;

import cn.net.susan.util.SnowFlakeIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 统一封装ID生成服务
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/21 下午5:27
 */
@Component
public class IdGenerateHelper {

    @Autowired
    private SnowFlakeIdWorker snowFlakeIdWorker;

    /**
     * 生成分布式ID
     *
     * @return 分布式ID
     */
    public Long nextId() {
        return snowFlakeIdWorker.nextId();
    }
}
